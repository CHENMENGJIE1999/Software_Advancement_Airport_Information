import { AMapScene } from '@antv/l7-react';
import * as React from 'react';
import { Drawer, Button, Menu, } from 'antd';
import {
    MenuUnfoldOutlined, MenuFoldOutlined, TableOutlined, ProjectOutlined, BarChartOutlined, LineChartOutlined, ScheduleOutlined
} from '@ant-design/icons';
import { APTDATA_TYPE } from './utils/enum'
import { OPTION_KEY, } from './utils/enum';
import TopTen from './getTopAT';
import ComAirNum from './getComAirNum';
import AirportList from './getAirportList';
import CThroungputList from './getCThroughputList';
import AvgDelayTimeList from './getAvgDelayTimeList';

const World = React.memo(function Map() {
    const [state, setState] = React.useState(0);
    const [collapsed, setCollapse] = React.useState(true);
    const [globalVisible, setGlobalVisible] = React.useState(false);
    const [STATUS, setSTATUS] = React.useState({});
    const [globalValues, setGlobalValues] = React.useState({});

    const [topTenPlaneData, settopTenPlaneData] = React.useState({});
    const [comAirNum, setComAirNum] = React.useState({});
    const [airportList, setAirportList] = React.useState({});
    const [cThroughputList, setCThroughputList] = React.useState({});
    const [avgDelayTimeList, setAvgDelayTimeList] = React.useState({});
    const getValues = (globalValue) => {
        setGlobalValues(globalValue);
        console.log(globalValues);
    }
    const getURL = () => {
        let tempURL = "";
        switch (state) {
            case 1:
                tempURL = APTDATA_TYPE.TOPTENPLANE.URL;
                break;
            case 2:
                tempURL = APTDATA_TYPE.COMAIRNUM.URL;
                break;
            case 3:
                tempURL = APTDATA_TYPE.AIRPORTLIST.URL;
                break;
            case 4:
                tempURL = APTDATA_TYPE.CTHROUGHPUTLIST.URL;
                break;
            case 5:
                tempURL = APTDATA_TYPE.AVGDELAYTIMELIST.URL;
                break;
            default:
                tempURL = "";
                break;
        }
        return tempURL;
    }
    const fetchURLData = async (url, para) => {
        const response = await fetch(url);
        const data = await response.json();
        const tempData = data.data.data;
        return tempData;
    }
    React.useEffect(async () => {
        await dealWithData();
        console.log(state);
    }, [state]);
    const dealWithData = async () => {
        const tempURl = getURL();
        const data = await fetchURLData(tempURl, globalValues);
        console.log("用户输入参数为：");
        console.log(globalValues);
        console.log("获取数据经处理为：");
        console.log(data);
        console.log(`此处state: ${state} `);
        const tempData = data;
        switch (state) {
            case 1:
                settopTenPlaneData(tempData || {});
                break;
            case 2:
                setComAirNum(tempData || {});
                break;
            case 3:
                setAirportList(tempData || {});
                break;
            case 4:
                setCThroughputList(tempData || {});
                break;
            case 5:
                setAvgDelayTimeList(tempData || {});
                break;
            default:
                break;
        }
    };
    const onGlobalClose = () => {
        setGlobalVisible(false);
    }
    const toggleCollapsed = () => {
        setCollapse(!collapsed);
    };
    const handleClickGlobal = (e) => {
        setGlobalVisible(true);
        const flag = e.key;
        switch (flag) {
            case "1":
                setState(1);
                setSTATUS(APTDATA_TYPE.TOPTENPLANE);
                break;
            case "2":
                setState(2);
                setSTATUS(APTDATA_TYPE.COMAIRNUM);
                break;
            case "3":
                setState(3);
                setSTATUS(APTDATA_TYPE.AIRPORTLIST);
                break;
            case "4":
                setState(4);
                setSTATUS(APTDATA_TYPE.CTHROUGHPUTLIST);
                break;
            case "5":
                setState(5);
                setSTATUS(APTDATA_TYPE.AVGDELAYTIMELIST);
                break;
            default:
                break;
        }
    }
    return (
        <>
            <AMapScene
                map={{
                    center: [116.6, 40.0773],
                    pitch: 0,
                    style: 'light',
                    zoom: 6,
                }}
                style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                }}
            >
                <div style={{ width: 256 }}>
                    <Button type="primary" onClick={toggleCollapsed} style={{ marginBottom: 16 }}>
                        {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined)}
                    </Button>
                    <Menu
                        defaultOpenKeys={['sub1']}
                        mode="inline"
                        theme="light"
                        inlineCollapsed={collapsed}
                    >
                        <Menu.Item key={OPTION_KEY.TOPTENPLANE} icon={<BarChartOutlined />}
                            onClick={handleClickGlobal}>
                            查询Top10机型
                        </Menu.Item>F
                        <Menu.Item key={OPTION_KEY.COMAIRNUM} icon={<LineChartOutlined />}
                            onClick={handleClickGlobal}
                        >
                            查询所有公司的航班数量
                        </Menu.Item>
                        <Menu.Item key={OPTION_KEY.AIRPORTLIST} icon={<TableOutlined />}
                            onClick={handleClickGlobal}>
                            分页查询机场
                        </Menu.Item>
                        <Menu.Item key={OPTION_KEY.CTHROUGHPUTLIST} icon={<ProjectOutlined />}
                            onClick={handleClickGlobal}>
                            查询各机场航班吞吐量
                        </Menu.Item>
                        <Menu.Item key={OPTION_KEY.AVGDELAYTIMELIST} icon={<ScheduleOutlined />}
                            onClick={handleClickGlobal}>
                            查询各航空公司平均晚点率
                        </Menu.Item>
                    </Menu>
                </div>

            </AMapScene>

            <Drawer
                title={STATUS.TITILE}
                key={STATUS.KEY}
                placement={STATUS.PLACEMANT}
                closable={STATUS.CLOSABLE}
                onClose={onGlobalClose}
                visible={globalVisible}
                width={STATUS.WIDTH}
            >
                {state === 1 && <TopTen STATUS={topTenPlaneData} />}
                {state === 2 && <ComAirNum STATUS={comAirNum} getValue={data => getValues(data)} />}
                {state === 3 && <AirportList STATUS={airportList} />}
                {state === 4 && <CThroungputList STATUS={cThroughputList} />}
                {state === 5 && <AvgDelayTimeList STATUS={avgDelayTimeList} />}
            </Drawer>
        </>
    );
});
export default World;

