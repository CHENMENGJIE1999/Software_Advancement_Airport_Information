
import React, { useState, useEffect } from 'react';//前12个城市飞入飞机数量与
import {
  Chart,
  Geom,
  Axis,
  Tooltip,
  Legend,
} from "bizcharts";
import DataSet from "@antv/data-set";
const Demo6: React.Component = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    asyncFetch();
  }, []);
  var url = 'http://flightapi.xiexianbo.xin/flight/getCityThroughputList?order=1&pageNo=1&pageSize=12';
  const asyncFetch = () => {
    fetch(url, {
      type: "GET",
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      dataType: "json",
    }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(data => {
        setData(data.data.data);
        console.log(data)
      })
  };
  const ds = new DataSet();
  const dv = ds.createView().source(data);
  dv.transform({
    type: "fold",
    fields: ["enterNum", "totol"],
    key: "key",
    value: "value"
  });
  const cols = {
    month: {
      range: [0, 1]
    }
  };
  return (
    <Chart height={600} appendPadding={[10, 0, 0, 0]} data={dv} scale={cols} autoFit>
      <Legend />
      <Axis name="month" />
      <Axis name="value" />
      <Tooltip
        crosshairs={{
          type: "y"
        }}
      />
      <Geom
        type="line"
        position="cityName*value"
        size={2}
        color={"key"}
        shape={"hv"}
      />
    </Chart>
  );
}


export default Demo6;
