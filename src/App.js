import { AMapScene, LoadImage, PointLayer, LayerEvent } from '@antv/l7-react';
import * as React from 'react';
import { Drawer,Descriptions } from 'antd';
import airport1 from './img/airport1.svg';
import airport2 from './img/airport2.svg';
import airport3 from './img/airport3.svg';
import airport4 from './img/airport4.svg';
import airport5 from './img/airport5.svg';

const World = React.memo(function Map() {
  const [data, setData] = React.useState();
  const [detailData, setDetailData] = React.useState();
  const [drawerVisible, setDrawerVisible] = React.useState(true);

  const onClose = () => {
    setDrawerVisible(false);
  };
  function handleClick(e) {
    console.log(e.feature);
    setDrawerVisible(true);
    setDetailData(e.feature);
  }

  React.useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(
        'http://flightapi.xiexianbo.xin/airPort/listAll'
      );
      const data = await response.json();
      setData(data.data.data);
    };
    fetchData();
  }, []);
  console.log(detailData)
  return (
    <>
      <AMapScene
        map={{
          center: [116.6, 40.0773],
          pitch: 0,
          style: 'light',
          zoom: 6,
        }}
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
        }}
      >
        <LoadImage name="00" url={airport1} />
        <LoadImage name="01" url={airport2} />
        <LoadImage name="02" url={airport3} />
        <LoadImage name="03" url={airport4} />
        <LoadImage name="04" url={airport5} />

        {data && (
          <PointLayer
            key={'2'}
            options={{
              // autoFit: true,
            }}
            source={{
              data,
              parser: {
                type: 'json',
                x: 'longitude',
                y: 'latitude',
              }
            }}
            shape={{
              field: 'flightNumber',
              values: (flightNumber) => {
                let status = '';
                if (flightNumber <= 100 && flightNumber >= 0) {
                  status = "00";
                } else if (flightNumber > 100 && flightNumber <= 500) {
                  status = '01';
                } else if (flightNumber > 500 && flightNumber <= 1000) {
                  status = '02';
                } else if (flightNumber > 1000) {
                  status = '03';
                } else {
                  status = '04';
                }
                return status;
              },
            }}
            size={{
              values: 10,
            }}
            style={{
              opacity: 0.8,
            }}
          >
            <LayerEvent type="click" handler={handleClick} />

          </PointLayer>
        )}

      </AMapScene>
      <Drawer
        title="Basic Drawer"
        placement="right"
        closable={false}
        onClose={onClose}
        visible={drawerVisible}
        width="900px"
      >
        <Descriptions>
          <Descriptions.Item label="IATACode" >{detailData.IATACode}</Descriptions.Item>
          <Descriptions.Item label="aptCcity">{detailData.aptCcity}</Descriptions.Item>
          <Descriptions.Item label="flightNumber">{detailData.flightNumber}</Descriptions.Item>
          <Descriptions.Item label="longitude">{detailData.longitude}</Descriptions.Item>
          <Descriptions.Item label="latitude">{detailData.latitude}</Descriptions.Item>
        </Descriptions>
      </Drawer>
    </>
  );
});
export default World;

