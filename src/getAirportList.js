import * as React from 'react';
import { Table, Button, Form, Row, Col, Input } from 'antd';

function ATSortList(props) {
    const { STATUS,getValue } = props;
    // 分页查询机场
    const [form] = Form.useForm();
    const getFields = () => {
        const children = [];
        const count = 2;
        const paraName = [ "pageNo", "pageSize"];
        for (let i = 0; i < count; i++) {
            children.push(
                <Col span={12} key={i}>
                    <Form.Item
                        name={`${paraName[i]}`}
                        label={`${paraName[i]}`}
                        ke={`${paraName[i]}`}
                        rules={[
                            {
                                required: true,
                                message: `please input ${paraName[i]}`,
                            },
                        ]}
                    >
                        <Input placeholder={"请输入" + paraName[i]} />
                    </Form.Item>
                </Col>,
            );
        }
        return children;
    };
    const onFinish = (values) => {
        getValue(values);
    };
    if (STATUS) {
        const data = Array.from(STATUS) || [];
        const columns = [
            {
                title: 'IATACode',
                dataIndex: 'IATACode',
                key: 'IATACode',
            },
            {
                title: 'country',
                dataIndex: 'country',
                key: 'country',
            }, {
                title: 'aptCity',
                dataIndex: 'aptCity',
                key: 'aptCity',
            }, {
                title: 'aptName',
                key: 'aptName',
                dataIndex: 'aptName',
                render: text => <a>{text}</a>,
            }, {
                title: 'aptCname',
                kay: 'aptCname',
                dataIndex: 'aptCname',
            },
        ];
        return (
            <>
                <Form
                    form={form}
                    name="advanced_search"
                    className="ant-advanced-search-form"
                    onFinish={onFinish}
                >
                    <Row gutter={24}>{getFields()}</Row>
                    <Row>
                        <Col
                            span={24}
                            style={{
                                textAlign: 'right',
                            }}
                        >
                            <Button type="primary" htmlType="submit">
                                Search
                            </Button>
                            <Button
                                style={{
                                    margin: '0 8px',
                                }}
                                onClick={() => {
                                    form.resetFields();
                                }}
                            >
                                Clear
                            </Button>
                        </Col>
                    </Row>
                </Form>
                <Table columns={columns} dataSource={data}></Table>

            </>
        )
    } else {
        return null;
    }
};


export default ATSortList;
