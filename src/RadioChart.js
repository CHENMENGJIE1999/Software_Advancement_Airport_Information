import { AMapScene, LineLayer, PointLayer } from '@antv/l7-react';
import * as React from 'react';
import ReactDOM from 'react-dom';
const Demo8 = React.memo(function Map() {
  const [data, setData] = React.useState();
  React.useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(
        'http://flightapi.xiexianbo.xin/airPort/listAll',
      );
      const data = await response.json();
      for (let i = 0; i < 266; i++) {
        data.data.data[i].longitude1 = 116.395645038;//向Json数据中加入新属性
        data.data.data[i].latitude1 = 39.9299857781;
      }
      setData(data.data.data);
      // console.log(data)
    };
    fetchData();
  }, []);
  return (<AMapScene
    map={{
      center: [108.76, 34.4365],
      pitch: 2,
      style: 'dark',
      zoom: 4.5
    }}
    style={{
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
    }}
  >
    {data && (
      <PointLayer
        source={{//与数据严格关联，看文档填写内容
          data,
          parser: {//解析数据的方式。
            type: 'json',
            x: 'longitude',//数据中的两个项
            y: 'latitude',
          }
        }}
        shape={{//图形形状
          values: 'circle',
        }}
        color={{
          values: ' #abc7e9'
        }}
        animate={{
          speed: 0.8
        }}
        size={{
          values: 30,
        }}
        style={{
          opacity: 1.0
        }}
      >
      </PointLayer>
    )}
    {data && (
      <LineLayer
        source={{//与数据严格关联，看文档填写内容
          data,
          parser: {//解析数据的方式。
            type: 'json',
            x: 'longitude1',//数据中的两个项
            y: 'latitude1',//把北京坐标，写进json数据！
            x1: 'longitude',
            y1: 'latitude'
          }
        }}
        shape={{//图形形状
          values: 'arc3d',
        }}
        color={{
          values: '#b97feb'
        }}
        active={{
          values: true
        }}
        size={{
          values: 2,
        }}
        style={{
          opacity: 1.0
        }}
        animate={{
          interval: 2,
          trailLength: 2,
          duration: 1
        }}
      >
      </LineLayer>
    )}
  </AMapScene>
  );
})
export default Demo8;
