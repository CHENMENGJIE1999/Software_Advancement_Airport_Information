import React, { useState, useEffect } from "react";//双轴图展示
import { Chart, Tooltip, Legend, Point, Line, Interval } from "bizcharts";

function Demo3() {
  const [data3, setData3] = useState(undefined);
  useEffect(() => {
    fetch('http://flightapi.xiexianbo.xin/flight/getCityThroughputList?order=1&pageNo=1&pageSize=20')
      .then(res => res.json())
      .then(data => {
        setData3(data.data.data);
        console.log(data3)
      })
  }, [])
  console.log(data3)
  // const data = [
  //     {
  //         time: "10:10",
  //         call: 4,
  //         waiting: 2,
  //         people: 2,
  //     }
  // ];
  let chartIns = null;
  const scale = {
    people: {
      alias: '飞入',
      type: 'linear-strict'
    },
    waiting: {
      alias: '飞出',
      type: 'linear-strict'
    },
  };
  const colors = ["#6394f9", "#62daaa"];
  return <Chart
    scale={scale}
    autoFit
    height={620}
    data={data3}
    onGetG2Instance={(chart) => {
      chartIns = chart;
      chartIns.on("interval:mouseenter", (e) => {
        chartIns.geometries.forEach((g) => {
          if (g.type === "interval") {
            (g.getShapes() || []).forEach((s) => {
              s.set("origin_fill", s.get("attrs").fill);
              s.attr("fill", "red");
            });
          }
        });
      });
      chartIns.on("interval:mouseleave", (e) => {
        console.log(chartIns);
        chartIns.geometries.forEach((g) => {
          if (g.type === "interval") {
            (g.getShapes() || []).forEach((s) => {
              s.attr("fill", s.get("origin_fill"));
            });
          }
        });
      });
    }}
  >
    <Legend
      custom={true}
      allowAllCanceled={true}
      items={[
        {
          value: "enterNum",
          name: "飞入",
          marker: {
            symbol: "square",
            style: { fill: colors[0], r: 5 },
          },
        },
        {
          value: "exitNum",
          name: "飞出",
          marker: {
            symbol: "hyphen",
            style: { stroke: colors[1], r: 5, lineWidth: 3 },
          },
        },
      ]}
      onChange={(ev) => {
        const item = ev.item;
        const value = item.value;
        const checked = !item.unchecked;
        const geoms = chartIns.geometries;

        for (let i = 0; i < geoms.length; i++) {
          const geom = geoms[i];

          if (geom.getYScale().field === value) {
            if (checked) {
              geom.show();
            } else {
              geom.hide();
            }
          }
        }
      }}
    />
    <Tooltip shared />
    <Interval position="cityName*enterNum" color={colors[0]} />
    <Line
      position="cityName*exitNum"
      color={colors[1]}
      size={3}
      shape="smooth"
    />
    <Point
      position="cityName*exitNum"
      color={colors[1]}
      size={3}
      shape="circle"
    />
  </Chart>
}
export default Demo3;
