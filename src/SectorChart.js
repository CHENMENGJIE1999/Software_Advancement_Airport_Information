import React, { useState, useEffect } from 'react';//扇形图，各航空公司拥有飞机数量及所占全部的比例
import { Pie } from '@ant-design/charts';

const Demo5: React.FC = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    asyncFetch();
  }, []);
  var url = 'http://flightapi.xiexianbo.xin/flight/getComAirNum?searchCtry=CN&pageNo=1&pageSize=50';
  const asyncFetch = () => {
    fetch(url, {
      type: "GET",
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      dataType: "json",
    }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(data => {
        setData(data.data.data);
        console.log(data)
      })
  };
  var config = {
    appendPadding: 10,
    data: data,
    angleField: 'airNumber',//依照飞机数量分配百分比
    colorField: 'airCName',//依照所有航空公司分配颜色
    radius: 1,//圆大小
    label: {
      type: 'spider',
      labelHeight: 28,
      content: '{name}\n{percentage}',
    },
    interactions: [{ type: 'element-selected' }, { type: 'element-active' }],
  };
  return <Pie {...config}
    height={620}
  />;
};

export default Demo5;