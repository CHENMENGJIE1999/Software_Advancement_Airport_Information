import { AMapScene, PointLayer } from '@antv/l7-react';
import * as React from 'react';

const Demo7 = React.memo(function Map() {
  const [data, setData] = React.useState();
  React.useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(
        'http://flightapi.xiexianbo.xin/airPort/listAll',
      );
      const data = await response.json();
      setData(data.data.data);
      console.log(data)
    };
    fetchData();
  }, []);
  return (<AMapScene
    map={{
      center: [108.76, 34.4365],
      pitch: 40,
      style: 'dark',
      zoom: 4.5
    }}
    style={{
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
    }}
  >
    {data && (
      <PointLayer
        source={{//与数据严格关联，看文档填写内容
          data,
          parser: {//解析数据的方式。
            type: 'json',
            x: 'longitude',//数据中的两个项
            y: 'latitude',
          }
        }}
        shape={{//图形形状
          values: 'cylinder',
        }}
        active={{
          values: true
        }}
        color={{
          field: 'flightNumber',
          values:
            ['#e8c65f',
              '#eed86c',
              '#e0ea5a',
              '#e1e748',
              '#f5f242',
              '#f67f16',
              '#fa692e',
              '#aa4b34',
              '#f55e44',
              '#fc3a08']
        }}
        size={{
          field: 'flightNumber',
          values: function (level) {
            return [1, 2, level / 10]
          }
        }}
        style={{
          opacity: 1.0
        }}
      >
      </PointLayer>
    )}
  </AMapScene>
  );
})
export default Demo7;
