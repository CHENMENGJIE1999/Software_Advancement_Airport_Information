
import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import {
  Chart,
  Geom,
  Tooltip,
  Coordinate,
  Legend,
  Axis,
  Interaction,
  G2,
  registerShape
} from "bizcharts";
import DataSet from "@antv/data-set";
function getTextAttrs(cfg) {
  return _.assign(
    {},
    cfg.style,
    {
      fontSize: cfg.data.size,
      text: cfg.data.text,
      textAlign: 'center',
      fontFamily: cfg.data.font,
      fill: cfg.color,
      textBaseline: 'Alphabetic'
    }
  );
}
registerShape("point", "cloud", {
  draw(cfg, container) {
    // console.log('cloud cfg', cfg);
    const attrs = getTextAttrs(cfg);
    const textShape = container.addShape("text", {
      attrs: _.assign(attrs, {
        x: cfg.x,
        y: cfg.y
      })
    });
    if (cfg.data.rotate) {
      G2.Util.rotate(textShape, cfg.data.rotate * Math.PI / 180);
    }
    return textShape;
  }
});
const Demo7: React.Component = () => {
  // const [data, setData] = React.useState();
  // React.useEffect(() => {
  //     const fetchData = async () => {
  //         const response = await fetch(
  //             'http://flightapi.xiexianbo.xin/airPort/flightSortlist?pageNo=1&pageSize=50',
  //         );
  //         const data = await response.json();
  //         setData(data.data.data);
  //          console.log(data)
  //     };
  //     fetchData();
  // }, []);
  const [data, setData] = useState([]);
  useEffect(() => {
    asyncFetch();
  }, []);
  var url = 'http://flightapi.xiexianbo.xin/airPort/flightSortlist?pageNo=1&pageSize=732';
  const asyncFetch = () => {
    fetch(url, {
      type: "GET",
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      dataType: "json",
    }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(data => {
        for (let i = 0; i < 50; i++) {//删除无效元素
          delete data.data.data[i].id;
          delete data.data.data[i].IATACode;
          delete data.data.data[i].country;
        }
        let colId = "flightNumber"
        let desc = function (x, y) {
          return (x[colId] < y[colId]) ? 1 : -1
        }
        data.data.data.sort(desc);
        setData(data.data.data);
      })
  };
  console.log(data)
  const dv = new DataSet.View().source(data);
  const min = 0;//data[49].flightNumber
  console.log(data[49])
  const max = 2893;////data[0].flightNumber 直接用的话会无法获取到data
  console.log(data[0])
  dv.transform({
    type: 'tag-cloud',
    fields: ['aptCname', 'flightNumber'],
    size: [600, 500],
    font: 'Verdana',
    padding: 0,
    timeInterval: 5000, // 最大执行时间
    rotate() {
      let random = ~~(Math.random() * 4) % 4;
      if (random === 2) {
        random = 0;
      }
      return random * 270; // 0, 90, 270
    },
    fontSize(d) {
      if (d.flightNumber) {
        return ((d.flightNumber - min) / (max - min)) * (40 - 12) + 12;
      }
      return 0;
    }
  });
  const scale = {
    x: {
      nice: false
    },
    y: {
      nice: false
    }
  }
  return (
    <Chart
      width={1300}
      height={650}
      data={dv}
      scale={scale}
      padding={0}
      autoFit={false}
      onPointClick={console.log}
    >
      <Tooltip showTitle={false} />
      <Coordinate reflect="y" />
      <Axis name='x' visible={false} />
      <Axis name='y' visible={false} />
      <Legend visible={false} />
      <Geom
        type='point'
        position="x*y"
        color="aptCcity"
        shape="cloud"
        tooltip="aptCcity*flightNumber"
      />
      <Interaction type='element-active' />
    </Chart>
  );
}


export default Demo7;
