import React, { useState, useEffect } from 'react';//带轴柱状图显示各机场飞行数量的直观比例 OK
import { Column } from '@ant-design/charts';

const Demo2: React.FC = () => {
  const [data2, setData2] = useState([]);
  useEffect(() => {
    asyncFetch();
  }, []);
  const asyncFetch = () => {
    fetch('http://flightapi.xiexianbo.xin/airPort/listAll')
      .then((response) => response.json())
      .then((json) => setData2(json.data.data))
      .catch((error) => {
        console.log('fetch data failed', error);
      });
  };
  let config = {
    data: data2,
    xField: 'aptCname',//横轴aptCname机场名字
    yField: 'flightNumber',//纵轴flightNumbe飞机吞吐量
    xAxis: { label: { autoRotate: false } },//旋转
    slider: {//滑标起始位置
      start: 0.1,
      end: 0.2,
    },
  };
  return <Column
    height={620}
    {...config} />;
};

export default Demo2;