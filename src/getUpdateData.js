export const getUpdateData = async (url, params) => {
    console.log("我来请求带参数据了");
    if (params) {
        let paramsArray = [];
        //拼接参数
        Object.keys(params).forEach(key => paramsArray.push(key + '=' + params[key]))
        if (url.search(/\?/) === -1) {
            url += '?' + paramsArray.join('&')
        } else {
            url += '&' + paramsArray.join('&')
        }
        console.log(url);
    }
    //fetch请求
    let updateData = [];
    await fetch(url, {
        method: 'GET',
    })
        .then((response) => response.json())
        .then((data) => {
            updateData = data;
        })
        .catch((error) => {
            alert(error)
        })
    console.log("请求带参数据结束");
    return updateData;
}