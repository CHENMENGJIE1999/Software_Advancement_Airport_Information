import React, { useState, useEffect } from 'react';//气泡图显示各机场飞行数量 OK
import {//按照省份
  Chart,
  Point,
  Legend,
  Axis
} from 'bizcharts';
import './App.css';
function Demo1() {
  const [data1, setData1] = useState(undefined);
  useEffect(() => {
    fetch('http://flightapi.xiexianbo.xin/airPort/listAll')
      .then(res => res.json())
      .then(data => {
        setData1(data.data.data);
        console.log(data1)
      })
  }, [])

  const scale = {
    IATACode: {
      alias: '机场代号',
      nice: true,
    },
    aptCcity: {
      // type: 'pow',
      alias: '所在城市'
    },
    aptCname: {
      alias: '机场名称',
      nice: true,
    },
    longitude: {
      alias: '经度'
    },
    latitude: {
      alias: '维度'
    },
    flightNumber: {
      alias: '飞机吞吐量'
    }

  };

  const colorMap = {
    乌鲁木齐地窝堡: '#f60707',
    花土沟: '#780505',
    阿坝红原: '#e07373',
    兰州中川: '#6d2f05',
    银川河东: '#d45832',
    乌特拉中旗: '#ec6b14',
    西安咸阳: '#de9948',
    成都双流: '#eacba5',
    重庆江北: '#383221',
    郑州新郑: '#e3e3df',
    昆明长水: '#4ff507',
    贵阳龙洞堡: '#c2f159',
    南宁吴圩: '#5f845a',
    海口美兰: '#7b5e3c',
    三亚凤凰: '#98bf3a',
    珠海金湾: '#a4cfef',
    香港: '#49d4e7',
    深圳宝安: '#1da3a3',
    广州白云: '#196c6a',
    厦门高崎: '#53d0a8',
    泉州晋江: '#c0adee',
    福州长乐: '#9883f3',
    温州龙湾: '#843ef1',
    南昌昌北: '#320f74',
    长沙黄花: '#f306e8',
    东阳横店机场: '#dd9de2',
    九江庐山: '#9e4fbb',
    宁波栎社: '#5c157b',
    杭州萧山: '#927b97',
    德清莫干山机场: '#ec1056',
    上海浦东: '#c62e5f',
    上海虹桥: '#c62e5f',
    武汉天河: '#65163b',
    南京禄口: '#c75fa5',
    合肥新桥: '#a73b74',
    青岛流亭: '#932f76',
    济南遥墙: '#b45a89',
    石家庄正定: '#f5e1e0',
    太原武宿: '#c78a82',
    烟台蓬莱: '#3b824a',
    大连周水子: '#165a05',
    天津滨海: '#7eeab8',
    北京大兴: 'rgba(6,34,227,0.99)',
    呼和浩特白塔: '#36a048',
    沈阳桃仙: '#5a5322',
    长春龙嘉: '#3b794d',
    巴林: '#373565',
    阿鲁科尔沁机场: '#743d8d',
    哈尔滨太平: '#534655',
    北大荒: '#715262',
    敖鲁古雅: '#784873',
    成吉思汗: '#531895',
  };

  return <Chart
    height={620}
    data={data1}
    autoFit
    scale={scale}
    interactions={['element-active']}
  >
    <Legend name="Population" visible={false} />
    <Point
      position="longitude*latitude"//位置
      color={["aptCname", val => {//依照colorMap中aptCname名称对应赋予颜色， 其他默认蓝色
        return colorMap[val];
      }]}
      opacity={0.65}//透明度
      shape="circle"
      size={["flightNumber", [2, 70]]}
      style={['continent', (val) => {
        return {
          lineWidth: 1,
          strokeOpacity: 1,//不透明度
          fillOpacity: 0.5,//填充不透明度
          opacity: 0.65,
          stroke: colorMap[val],
        };
      }]}
    />
    <Axis name={"latitude"} grid={{
      line: {
        style: {
          stroke: '#e3e3e3'
        }
      }
    }} />
    <Axis name={"longitude"} grid={{
      line: {
        style: {
          stroke: '#e3e3e3'
        }
      }
    }} />
  </Chart>
}

export default Demo1;