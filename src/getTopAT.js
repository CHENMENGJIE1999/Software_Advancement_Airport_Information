import * as React from 'react';
import { Column } from '@ant-design/charts';
import { Empty, } from 'antd';
function TopTen(props) {
    const { STATUS } = props;
    if (STATUS) {
        var data = Array.from(STATUS) || [];
        // var data = STATUS||[];
        var config = {
            autoFit: 'true',
            data: data,
            xField: 'atype',
            yField: 'flightAmount',
            label: {
                position: 'middle',
                style: {
                    fill: '#FFFFFF',
                    opacity: 0.6,
                },
            },
            xAxis: {
                label: {
                    autoHide: true,
                    autoRotate: false,
                },
            },
            meta: {
                atype: { alias: '机型' },
                flightAmount: { alias: '航班数量' },
            },
        };
        return (
            <Column {...config} /> || <Empty />
        );
    }
    else {
        return null;
    }
};
export default TopTen;
