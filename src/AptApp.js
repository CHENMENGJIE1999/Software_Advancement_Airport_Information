import airport1 from './img/airport1.svg';
import airport2 from './img/airport2.svg';
import airport3 from './img/airport3.svg';
import airport4 from './img/airport4.svg';
import airport5 from './img/airport5.svg';
import { AMapScene, LoadImage, PointLayer, LayerEvent } from '@antv/l7-react';
import * as React from 'react';
import { Drawer, Button, Menu } from 'antd';
import {
  MenuUnfoldOutlined, MenuFoldOutlined, TableOutlined, ProjectOutlined, ScheduleOutlined,
  LineChartOutlined, PieChartOutlined, ForkOutlined, AreaChartOutlined, RedoOutlined, ColumnWidthOutlined,
  CloudOutlined, DeploymentUnitOutlined, CodepenOutlined,
} from '@ant-design/icons';
import AptInfo from './showAptDetailInfo';
import { APTDATA_TYPE } from './utils/enum'
import { OPTION_KEY, } from './utils/enum';
import { getUpdateData } from './getUpdateData';
import TopTen from './getTopAT';
import ComAirNum from './getComAirNum';
import AirportList from './getAirportList';
import CThroungputList from './getCThroughputList';
import AvgDelayTimeList from './getAvgDelayTimeList';
import Bubblechart from './Bubblechart';
import ColumnChart from './ColumnChart';
import DoubleColumnChart from './DoubleColumnChart';
import BrokednLineChart from './BrokednLineChart';
import SectorChart from './SectorChart';
import DoubleBrokednLineChart from './DoubleBrokednLineChart';
import WordCloudChart from './WordCloudChart';
import RadioChart from './RadioChart';
import ThreeDColunChart from './ThreeDColunChart';


const World = React.memo(function Map() {
  // 地图飞机场详细信息
  const [data, setAllAptData] = React.useState({});
  const [detailData, setDetailData] = React.useState({});
  const [drawerVisible, setDrawerVisible] = React.useState(false);
  // 控制显示哪一个航班信息
  const [state, setState] = React.useState(0);
  const [collapsed, setCollapse] = React.useState(false);
  // 控制通用Drawer的可见与否
  const [globalVisible, setGlobalVisible] = React.useState(false);
  // 控制通用Drawer的样式
  const [STATUS, setSTATUS] = React.useState({});
  // 子组件传回来的用户输入参数
  const [globalValues, setGlobalValues] = React.useState({});
  // 5列航班有关信息数据存储
  const [topTenPlaneData, settopTenPlaneData] = React.useState({});
  const [comAirNum, setComAirNum] = React.useState({});
  const [airportList, setAirportList] = React.useState({});
  const [cThroughputList, setCThroughputList] = React.useState({});
  const [avgDelayTimeList, setAvgDelayTimeList] = React.useState({});

  // 控制显示数据流向图的Drawer
  const [updateDrawerVisible, setupdateDrawerVisible] = React.useState(false);

  const [temp, setTemp] = React.useState();
  // 获取地图航班信息
  React.useEffect(() => {
    let url = 'http://flightapi.xiexianbo.xin/airPort/listAll';
    console.log("调用了获取整张地图数据的useEffect()")
    const fetchData = async () => {
      const response = await fetch(url);
      const data = await response.json();
      setAllAptData(data.data.data);
    };
    fetchData();
  }, []);

  // 获取数据
  const fetchURLData = async (url) => {
    console.log("根据获取到的url调用了useEffect()，此时无参")
    const response = await fetch(url);
    const data = await response.json();
    const tempData = data.data.data;
    return tempData;
  }
  React.useEffect(async () => {
    await dealWithData();
  }, [state]);

  // 获取并处理数据，更新每个组件的原始数据
  const dealWithData = async () => {
    const tempURl = getURL();
    const data = await fetchURLData(tempURl);
    console.log(`此处state: ${state} `);
    const tempData = data;
    switch (state) {
      case 1:
        settopTenPlaneData(tempData || {});
        break;
      case 2:
        setComAirNum(tempData || {});
        break;
      case 3:
        setAirportList(tempData || {});
        break;
      case 4:
        setCThroughputList(tempData || {});
        break;
      case 5:
        setAvgDelayTimeList(tempData || {});
        break;
      default:
        break;
    }
  };

  // 整张地图信息Drawer关闭
  const onClose = () => {
    setDrawerVisible(false);
  };

  // 控制global dDrawer的显示与否
  const onGlobalClose = () => {
    setGlobalVisible(false);
  }

  // 关闭数据流图Drawer
  const onCloseUpdetaDrawer = () => {
    setupdateDrawerVisible(false);
  }

  // 旁边的一栏menu是否塌陷
  const toggleCollapsed = () => {
    setCollapse(!collapsed);
  };

  // 点击显示航班详细信息
  function handleShowDes(e) {
    setDrawerVisible(true);
    setDetailData(e.feature);
  }

  // 得到子组件用户输入的参数值
  const getValues = async (globalValue) => {
    setGlobalValues(globalValue);
    let currentURL = getURL();//得到当前URL
    let updateData = [];
    let data = [];
    updateData = await getUpdateData(currentURL, globalValues);
    // 得到重新获取的数据
    console.log(updateData);
    data = updateData.data.data || [];
    switch (state) {
      case 2:
        setComAirNum(data);//更新hooks的数据
        break;
      case 3:
        setAirportList(data);
        break;
      case 4:
        setCThroughputList(data);
        break;
      case 5:
        setAvgDelayTimeList(data);
        break;
      default:
        break;
    }
  }

  // 根据state获取url
  const getURL = () => {
    let tempURL = "";
    switch (state) {
      case 1:
        tempURL = APTDATA_TYPE.TOPTENPLANE.URL;
        break;
      case 2:
        tempURL = APTDATA_TYPE.COMAIRNUM.URL;
        break;
      case 3:
        tempURL = APTDATA_TYPE.AIRPORTLIST.URL;
        break;
      case 4:
        tempURL = APTDATA_TYPE.CTHROUGHPUTLIST.URL;
        break;
      case 5:
        tempURL = APTDATA_TYPE.AVGDELAYTIMELIST.URL;
        break;
      default:
        tempURL = "";
        break;
    }
    return tempURL;
  }

  // 点击menu.item选择获取数据
  const handleClickGlobal = (e) => {
    setGlobalVisible(true);
    const flag = e.key;
    switch (flag) {
      case "1":
        setState(1);
        setSTATUS(APTDATA_TYPE.TOPTENPLANE);
        break;
      case "2":
        setState(2);
        setSTATUS(APTDATA_TYPE.COMAIRNUM);
        break;
      case "3":
        setState(3);
        setSTATUS(APTDATA_TYPE.AIRPORTLIST);
        break;
      case "4":
        setState(4);
        setSTATUS(APTDATA_TYPE.CTHROUGHPUTLIST);
        break;
      case "5":
        setState(5);
        setSTATUS(APTDATA_TYPE.AVGDELAYTIMELIST);
        break;
      default:
        break;
    }
  };

  const showBubbleChart = () => {
    setTemp(1);
    setupdateDrawerVisible(true);
    return temp;
  }

  const showColumnChart = () => {
    setTemp(2);
    setupdateDrawerVisible(true);
    return temp;
  }

  const showDoubleColumn = () => {
    setTemp(3);
    setupdateDrawerVisible(true);
    return temp;
  }

  const showBrokenLineChart = () => {
    setTemp(4);
    setupdateDrawerVisible(true);
    return temp;
  }

  const showSectorChart = () => {
    setTemp(5);
    setupdateDrawerVisible(true);
    return temp;
  }

  const showDoubleBrokenLineChart = () => {
    setTemp(6);
    setupdateDrawerVisible(true);
    return temp;
  }

  const showWordCloudChart = () => {
    setTemp(7);
    setupdateDrawerVisible(true);
    return temp;
  }

  const showRadioChart = () => {
    setTemp(8);
    setupdateDrawerVisible(true);
    return temp;
  }

  const showThreeDColumnChart = () => {
    setTemp(9);
    setupdateDrawerVisible(true);
    return temp;
  }

  return (
    <>
      <AMapScene
        map={{
          center: [104.72, 31.4287],
          pitch: 0,
          style: 'light',
          zoom: 6,
        }}
        style={{
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
        }}
      >

        <div style={{ width: 270,fontSize:10 }}>
          <Button type="primary" onClick={toggleCollapsed} style={{ marginBottom: 16 }}>
            {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined)}
          </Button>
          <Menu
            defaultOpenKeys={['sub1']}
            mode="inline"
            theme="light"
            inlineCollapsed={collapsed}
          >
            <Menu.Item key={OPTION_KEY.COMAIRNUM} icon={<LineChartOutlined />}
              onClick={handleClickGlobal}
            >
              公司航班数量
            </Menu.Item>
            <Menu.Item key={OPTION_KEY.AIRPORTLIST} icon={<TableOutlined />}
              onClick={handleClickGlobal}>
              查询机场
            </Menu.Item>
            <Menu.Item key={OPTION_KEY.CTHROUGHPUTLIST} icon={<ProjectOutlined />}
              onClick={handleClickGlobal}>
              机场航班吞吐量
            </Menu.Item>
            <Menu.Item key={OPTION_KEY.AVGDELAYTIMELIST} icon={<ScheduleOutlined />}
              onClick={handleClickGlobal}>
              平均晚点率
            </Menu.Item>
            <Menu.Item key="1" icon={<RedoOutlined />} onClick={showBubbleChart}>机场飞行数</Menu.Item>
            <Menu.Item key="2" icon={<ColumnWidthOutlined />} onClick={showColumnChart}>机场飞行数量直观比例</Menu.Item>
            <Menu.Item key="3" icon={<ProjectOutlined />} onClick={showDoubleColumn} >吞吐量前20城市飞机起落数</Menu.Item>
            <Menu.Item key="4" icon={<AreaChartOutlined />} onClick={showBrokenLineChart} >每日航班飞行数量与平均数对比</Menu.Item>
            <Menu.Item key="5" icon={<PieChartOutlined />} onClick={showSectorChart}>公司拥有飞机数所占比例</Menu.Item>
            <Menu.Item key="6" icon={<ForkOutlined />} onClick={showDoubleBrokenLineChart} >12个热门城市航班数对比</Menu.Item>
            <Menu.Item key="7" icon={<CloudOutlined />} onClick={showWordCloudChart}>机场吞吐量大小对比</Menu.Item>
            <Menu.Item key="8" icon={<DeploymentUnitOutlined />} onClick={showRadioChart}>北京到各地航班流向</Menu.Item>
            <Menu.Item key="9" icon={<CodepenOutlined />} onClick={showThreeDColumnChart}>地区分布及航班数量对比</Menu.Item>
          </Menu>
        </div>

        <LoadImage name="00" url={airport1} />
        <LoadImage name="01" url={airport2} />
        <LoadImage name="02" url={airport3} />
        <LoadImage name="03" url={airport4} />
        <LoadImage name="04" url={airport5} />
        {data && (
          <PointLayer
            key={'2'}
            options={{
            }}
            source={{
              data,
              parser: {
                type: 'json',
                x: 'longitude',
                y: 'latitude',
              }
            }}
            shape={{
              field: 'flightNumber',
              values: (flightNumber) => {
                let status = '';
                if (flightNumber <= 100 && flightNumber >= 0) {
                  status = "00";
                } else if (flightNumber > 100 && flightNumber <= 500) {
                  status = '01';
                } else if (flightNumber > 500 && flightNumber <= 1000) {
                  status = '02';
                } else if (flightNumber > 1000) {
                  status = '03';
                } else {
                  status = '04';
                }
                return status;
              },
            }}
            size={{
              values: 10,
            }}
            style={{
              opacity: 0.8,
            }}
            active={{
              option: {
                color: 'pink',
              },
            }}
          >
            <LayerEvent type="click" handler={handleShowDes} />
          </PointLayer>
        )}
      </AMapScene>
      {/* 显示航班详细信息 */}
      <Drawer
        title=""
        placement="right"
        closable={true}
        onClose={onClose}
        visible={drawerVisible}
        width="700px"
      >
        <AptInfo detailData={detailData} />
      </Drawer>

      <Drawer
        title={STATUS.TITILE}
        key={STATUS.KEY}
        placement={STATUS.PLACEMANT}
        closable={STATUS.CLOSABLE}
        onClose={onGlobalClose}
        visible={globalVisible}
        width={STATUS.WIDTH}
      >
        {state === 1 && <TopTen STATUS={topTenPlaneData} />}
        {state === 2 && <ComAirNum STATUS={comAirNum} getValue={data => getValues(data)} />}
        {state === 3 && <AirportList STATUS={airportList} getValue={data => getValues(data)} />}
        {state === 4 && <CThroungputList STATUS={cThroughputList} getValue={data => getValues(data)} />}
        {state === 5 && <AvgDelayTimeList STATUS={avgDelayTimeList} getValue={data => getValues(data)} />}
      </Drawer>

      <Drawer
        placement="right"
        closable={true}
        onClose={onCloseUpdetaDrawer}
        visible={updateDrawerVisible}
        width="1200px"
      >
        {(() => {
          switch (temp) {
            case 1:
              return <div className={'Name'} >气泡图显示各飞机场飞行数量<Bubblechart /></div>
            case 2:
              return <div className={'Name'}>带轴柱状图显示中国各机场飞行数量的直观比例<ColumnChart /></div>
            case 3:
              return <div className={'Name'}>双轴图展示吞吐量前20的中国城市飞机起落数量 <DoubleColumnChart /></div>
            case 4:
              return <div className={'Name'}>折线图显示每日航班飞行数量及与平均数的对比<BrokednLineChart /></div>
            case 5:
              return <div className={'Name'}>扇形图显示各航空公司拥有飞机数量及所占全部的比例<SectorChart /></div>
            case 6:
              return <div className={'Name'} >双折线图显示12个热门城市的客机总吞吐量和仅驶入数量<DoubleBrokednLineChart /></div>
            case 7:
              return <div className={'Name1 Name2'}>词云图显示各机场相对吞吐量大小<WordCloudChart /></div>
            case 8:
              return <div className={'Name'} >流向图显示由首都北京向各地机场的辐射情况<RadioChart /></div>
            case 9:
              return <div className={'Name'}>3D柱图直观显示地区分布情况及航班飞行数量对比<ThreeDColunChart /></div>
            default:
              return null
          }
        }
        )()}
      </Drawer>
    </>
  );
});
export default World;

