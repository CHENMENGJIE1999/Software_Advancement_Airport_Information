import * as React from 'react';
import {  Line } from '@ant-design/charts';
import { Empty, Button, Form, Row, Col, Input } from 'antd';
function ComAirNUm(props) {
    // 分页查询所有公司的航班数量（可输入国家筛选）
    const { STATUS, getValue } = props;
    const [form] = Form.useForm();
    const getFields = () => {
        const children = [];
        const count = 3;
        const paraName = ["所在城市", "页数", "一页所含数据"];
        for (let i = 0; i < count; i++) {
            children.push(
                <Col span={8} key={i}>
                    <Form.Item
                        name={`${paraName[i]}`}
                        label={`${paraName[i]}`}
                        key={`${paraName[i]}`}
                        rules={[
                            {
                                required: true,
                                message: `please input ${paraName[i]}`,
                            },
                        ]}
                    >
                        <Input placeholder={"请输入" + paraName[i]} />
                    </Form.Item>
                </Col>,
            );
        }
        return children;
    };
    const onFinish = (values) => {
        console.log(values)
        getValue(values)
    };
    if (STATUS) {
        var data = Array.from(STATUS) || [];
        const config = {
            data,
            title: {
                visible: true,
                // text: '带数据点的折线图',
            },
            xField: 'airCName',
            yField: 'airNumber',
            point: {
                visible: true,
                size: 5,
                shape: 'diamond',
                style: {
                    fill: 'white',
                    stroke: '#2593fc',
                    lineWidth: 2,
                },
            },
        };

        return (
            <>
                <Form
                    form={form}
                    name="advanced_search"
                    className="ant-advanced-search-form"
                    onFinish={onFinish}
                >
                    <Row gutter={24}>{getFields()}</Row>
                    <Row>
                        <Col
                            span={24}
                            style={{
                                textAlign: 'right',
                            }}
                        >
                            <Button type="primary" htmlType="submit">
                                Search
                            </Button>
                            <Button
                                style={{
                                    margin: '0 8px',
                                }}
                                onClick={() => {
                                    form.resetFields();
                                }}
                            >
                                Clear
                            </Button>
                        </Col>
                    </Row>
                </Form>
                {<Line {...config} /> || <Empty />}
            </>
        );
    } else {
        return null;
    }
};


export default ComAirNUm;
