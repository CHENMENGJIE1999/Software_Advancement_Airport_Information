export const OPTION_KEY = {
    TOPTENPLANE: "1",
    COMAIRNUM: "2",
    AIRPORTLIST: "3",
    CTHROUGHPUTLIST: "4",
    AVGDELAYTIMELIST: "5",
    
};
export const APTDATA_TYPE = {
    // 前十机型
    TOPTENPLANE: {
        URL: 'http://flightapi.xiexianbo.xin/flight/getTopAT',
        KEY: '1',
        TITILE: 'Top10机型查询',
        PLACEMANT: 'right',
        WIDTH: "900px",
        CLOSABLE: "true",
    },
    // 分页查询所有公司的航班数量（可输入国家筛选）
    COMAIRNUM: {
        URL: 'http://flightapi.xiexianbo.xin/flight/getComAirNum',
        KEY: '2',
        TITILE: '查询所有公司的航班数量',
        PLACEMANT: 'right',
        WIDTH: "1000px",
        CLOSABLE: "true",
    },
    // 分页查询机场
    AIRPORTLIST: {
        URL: 'http://flightapi.xiexianbo.xin/airPort/list',
        KEY: '3',
        TITILE: '分页查询机场',
        PLACEMANT: 'right',
        WIDTH: "1000px",
        CLOSABLE: "true",
    },
    // 查询航班吞吐量
    // startTime endTime order pageNo pageSize
    CTHROUGHPUTLIST: {
        URL: 'http://flightapi.xiexianbo.xin/airPort/getCThroughputList',
        KEY: '4',
        TITILE: '查询各机场航班吞吐量',
        PLACEMANT: 'right',
        WIDTH: "1000px",
        CLOSABLE: "true",
    },
    // 查询各航空公司平均晚点时长，晚点率
    // startTime endTime order ctryDomainName pageNo pageSize
    AVGDELAYTIMELIST: {
        URL: 'http://flightapi.xiexianbo.xin/flight/getAvgDelayTimeList',
        KEY: '5',
        TITLE: '查询各航空公司平均晚点时长，晚点率',
        PLACEMANT: 'right',
        WIDTH: '1000px',
        CLOSABLE: "true",
    }
};
// 前十机型
// 分页查询所有公司的航班数量（可输入国家筛选）
// 分页查询机场
 // 查询航班吞吐量
// 查询各航空公司平均晚点时长，晚点率