import * as React from 'react';
import { Table, Button, Form, Row, Col, Input, DatePicker,  } from 'antd';
import moment from 'moment';
const { RangePicker } = DatePicker;

function CThrougnputList(props) {
    const { STATUS, getValue } = props;
    const [form] = Form.useForm();
    // 查询航班吞吐量

    const onFinish = (values) => {
        const { rangePicker } = values;
        let startTime = moment(rangePicker[0]).format('YYYY-MM-DD');
        let endTime = moment(rangePicker[1]).format('YYYY-MM-DD');
        let tempValues = values;
        tempValues.startTime = startTime;
        tempValues.endTime = endTime;
        // console.log(tempValues)
        delete tempValues.rangePicker
        console.log(tempValues);
        getValue(tempValues);
    };
  
    if (STATUS) {
        const data = Array.from(STATUS) || [];
        const columns = [
            {
                title: 'aptName',
                dataIndex: 'aptName',
                key: 'aptName',
                render: text => <a>{text}</a>,
            },
            {
                title: 'aptCcity',
                dataIndex: 'aptCcity',
                key: 'aptCcity',
            },
            {
                title: 'enterNum',
                dataIndex: 'enterNum',
                key: 'enterNum',
            }, {
                title: 'exitNum',
                dataIndex: 'exitNum',
                key: 'exitNum',
            }, {
                title: 'total',
                key: 'total',
                dataIndex: 'total',
                sorter: (a, b) => a.total - b.total,
                render: text => <a>{text}</a>,
            },
        ];
        // const paraName = ["startTime", "endTime", "order", "pageNo", "pageSize",];
        const rangeConfig = {
            rules: [
                {
                    type: 'array',
                    required: true,
                    message: 'Please select time!',
                },
            ],
        };
        return (
            <>
                <Form
                    form={form}
                    name="advanced_search"
                    className="ant-advanced-search-form"
                    onFinish={onFinish}
                >
                    <Row>
                        <Col span={8} key={"pageNo"}>
                            <Form.Item
                                name="pageNo"
                                label="pageNo"
                                rules={[
                                    {
                                        required: true,
                                        message:"please input pageNo!"
                                    },
                                ]}
                            ><Input placeholder="请输入pageNo" />
                            </Form.Item>
                        </Col>
                        <Col span={8} key={"pageSize"}>
                            <Form.Item
                                name="pageSize"
                                label="pageSize"
                                rules={[
                                    {
                                        required: true,
                                        message:'please input pageSize!'
                                    },
                                ]}
                            ><Input placeholder="请输入pageSize" />
                            </Form.Item>
                        </Col>
                        <Col span={8} key={"order"}>
                            <Form.Item
                                name="order"
                                label="order"
                                rules={[
                                    {
                                        required: true,
                                        message:'please input order'
                                    },
                                ]}
                            ><Input placeholder="请输入order" />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Form.Item name="时间选择" label="RangePicker" {...rangeConfig}>
                                <RangePicker />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row>
                        <Col
                            span={24}
                            style={{
                                textAlign: 'right',
                            }}
                        >
                            <Button type="primary" htmlType="submit">
                                Search
                            </Button>
                            <Button
                                style={{
                                    margin: '0 8px',
                                }}
                                onClick={() => {
                                    form.resetFields();
                                }}
                            >
                                Clear
                            </Button>
                        </Col>
                    </Row>
                </Form>
                <Table columns={columns} dataSource={data}></Table>
            </>
        )
    } else {
        return null;
    }
}
export default CThrougnputList;
