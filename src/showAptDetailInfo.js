import * as React from 'react';
import { Descriptions } from 'antd';
function AptInfo(props) {
    const { detailData } = props;
    if (detailData) {
        return (
            <>
                <Descriptions
                    title="航班详细信息"
                    bordered
                    column={4}
                >
                    {
                        Object.keys(detailData).map(item => {
                            let flag = '';
                            if (item === '_id') {
                                return ("");
                            }
                            switch (item) {
                                case 'IATACode':
                                    flag = '机场代码';
                                    break;
                                case 'aptCcity':
                                    flag = '所在城市';
                                    break;
                                case 'aptCname':
                                    flag = '机场名称';
                                    break;
                                case 'flightNumber':
                                    flag = '航班数';
                                    break;
                                case 'longitude':
                                    flag = '经度';
                                    break;
                                case 'latitude':
                                    flag = '纬度';
                                    break;
                                case 'coordinates':
                                    flag = '坐标';
                                    break;
                                default:
                                    break;
                            }
                            return (
                                <Descriptions.Item label={flag} span={2} key={item}>{detailData[item]}</Descriptions.Item>
                            )
                        })
                    }
                </Descriptions>
            </>
        );
    }
    else {
        alert("Error,catch data failed!")
    }

};
export default AptInfo;
