import React, { useState, useEffect } from 'react';//折线图显示每日航班飞行数量及与平均数的对比
import { Line } from '@ant-design/charts';

const Demo4: React.FC = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    asyncFetch();
  }, []);
  var url = 'http://flightapi.xiexianbo.xin/flight/getFlightDateInfo?pageNo=1&pageSize=35';
  const asyncFetch = () => {
    fetch(url, {
      type: "GET",
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      dataType: "json",
    }).then(res => res.json())
      .catch(error => console.error('Error:', error))
      .then(data => {
        setData(data.data.data);
        console.log(data)
      })
  };
  var config = {
    data: data,
    padding: 'auto',
    xField: 'flightDate',
    yField: 'flightNum',
    annotations: [
      {
        type: 'regionFilter',
        start: ['min', 'median'],
        end: ['max', '0'],
        color: '#F4664A',
      },
      {
        type: 'text',
        position: ['min', 'median'],
        content: '中位数',
        offsetY: -4,
        style: { textBaseline: 'bottom' },
      },
      {
        type: 'line',
        start: ['min', 'median'],
        end: ['max', 'median'],
        style: {
          stroke: '#F4664A',
          lineDash: [2, 2],
        },
      },
    ],
  };
  return <Line {...config}
    height={620}
  />;
};

export default Demo4;